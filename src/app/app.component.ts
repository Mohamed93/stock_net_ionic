import { Component, ViewChild } from '@angular/core';
import { Events, Platform, Nav, MenuController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SocialSharing } from '@ionic-native/social-sharing';

//For Translation
import { TranslateService } from '@ngx-translate/core';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
  rootPage:any = "LoginPage";

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    menu: MenuController, 
    public event :Events,
    private translate: TranslateService,
    private socShare: SocialSharing,
    public toastCtrl: ToastController) {

    this.translate.setDefaultLang("ar");
    this.translate.use("ar");
    // app lang
    translate.setDefaultLang(localStorage.getItem('strock_lang'));
    this.translate.setDefaultLang(localStorage.getItem('strock_lang'));
    this.translate.use(localStorage.getItem('strock_lang'));

    this.event.subscribe('change_lang',()=>{
      console.log('event run')
    // app lang
    translate.setDefaultLang(localStorage.getItem('strock_lang'));
    this.translate.setDefaultLang(localStorage.getItem('strock_lang'));
    this.translate.use(localStorage.getItem('strock_lang'));
    })


    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      menu.enable(true);
    });
  }

  openTabs(){
    this.navCtrl.push("TabsPage");
  }
  openWallet(){
    this.navCtrl.push("MyWalletPage");
  }
  openTerms(){
    this.navCtrl.push("TermsPage");
  }
  openCall(){
    this.navCtrl.push("CallUsPage");
  }
  openAbout(){
    this.navCtrl.push("AboutAppPage");
  }
  openSetting(){
    this.navCtrl.push("SettingPage");
  }
  openLogin(){
    this.navCtrl.push("LoginPage");
  }
  openShare(){
    this.socShare.share("All Share Options", null/*Subject*/, null/*File*/, "https://en.wikipedia.org/wiki/Main_Page")
    .then(() => {
    //here you can put a toast to inform the user of the suucess
    //of the sharing process 
    },
    () => {
    let toast = this.toastCtrl.create({
    cssClass: "myAlert",
    message: 'فشلت عملية المشاركة، يبدو أن هناك خطأ في الاتصال',
    duration: 4000
    });
    toast.present();
    })
  }

}

