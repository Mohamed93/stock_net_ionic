import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyWalletPage } from './my-wallet';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MyWalletPage,
  ],
  imports: [
    IonicPageModule.forChild(MyWalletPage),
    TranslateModule,
  ],
})
export class MyWalletPageModule {}
