import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ModalController } from 'ionic-angular';

/**
 * Generated class for the MyWalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-wallet',
  templateUrl: 'my-wallet.html',
})
export class MyWalletPage {
  iconRight : any = 'right';
  ionViewWillEnter(){
    if(this.platform.isRTL){
       this.iconRight = 'right';
    } else{
     this.iconRight = 'left';
    } 
   
 }
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private platform : Platform,
    public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyWalletPage');
  }

  openPopup(){
    const modal = this.modalCtrl.create("SelectPaymentPage");
    modal.present();
  }

}
