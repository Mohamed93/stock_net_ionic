import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromoCodePage } from './promo-code';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PromoCodePage,
  ],
  imports: [
    IonicPageModule.forChild(PromoCodePage),
    TranslateModule,
  ],
})
export class PromoCodePageModule {}
