import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';
import { Camera, CameraOptions } from '@ionic-native/camera';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  pet: string = "plus2";
  public base64Image: string;
  public photos: any;
  regData = { avatar:''};
  imgPreview = 'assets/imgs/sh3wza.jpg';
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private imagePicker:  ImagePicker,
    private base64:       Base64,
    private camera:       Camera,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  openPopup(){
    const modal = this.modalCtrl.create("EditViewPage");
    modal.present();
  }

  takePhoto() {
    const options : CameraOptions = {
    quality: 50, // picture quality
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    sourceType : this.camera.PictureSourceType.PHOTOLIBRARY,
    mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options) .then((imageData) => {
    this.imgPreview = "data:image/jpeg;base64," + imageData;
    // this.photos.push(this.base64Image);
    // this.photos.reverse();
    }, (err) => {
    console.log(err);
    });
  }

}
