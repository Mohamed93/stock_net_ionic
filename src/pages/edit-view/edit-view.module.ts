import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditViewPage } from './edit-view';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    EditViewPage,
  ],
  imports: [
    IonicPageModule.forChild(EditViewPage),
    TranslateModule,
  ],
})
export class EditViewPageModule {}
