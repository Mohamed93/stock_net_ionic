import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  
  tab1Root    = "HomePage";
  tab2Root    = "MyOrderPage";
  tab3Root    = "AddPointPage";
  tab4Root    = "MyWalletPage";
  tab5Root    = "ProfilePage";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
