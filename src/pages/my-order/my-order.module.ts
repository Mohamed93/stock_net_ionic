import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyOrderPage } from './my-order';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MyOrderPage,
  ],
  imports: [
    IonicPageModule.forChild(MyOrderPage),
    TranslateModule,
  ],
})
export class MyOrderPageModule {}
