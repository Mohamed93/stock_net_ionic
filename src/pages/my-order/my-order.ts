import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

/**
 * Generated class for the MyOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-order',
  templateUrl: 'my-order.html',
})
export class MyOrderPage {
  iconRight : any = 'right';
  ionViewWillEnter(){
    if(this.platform.isRTL){
       this.iconRight = 'right';
    } else{
     this.iconRight = 'left';
    } 
   
 }
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private platform : Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyOrderPage');
  }

}
