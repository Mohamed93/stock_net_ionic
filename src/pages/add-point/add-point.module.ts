import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPointPage } from './add-point';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AddPointPage,
  ],
  imports: [
    IonicPageModule.forChild(AddPointPage),
    TranslateModule,
  ],
})
export class AddPointPageModule {}
