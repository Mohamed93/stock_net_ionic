import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectPaymentPage } from './select-payment';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SelectPaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectPaymentPage),
    TranslateModule,
  ],
})
export class SelectPaymentPageModule {}
