import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

/**
 * Generated class for the SelectPaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-payment',
  templateUrl: 'select-payment.html',
})
export class SelectPaymentPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectPaymentPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  openPayment(){
    this.viewCtrl.dismiss();
    const modal = this.modalCtrl.create("PaymentPage");
    modal.present();
  }

}
