import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, Events, MenuController } from 'ionic-angular';

import {TranslateService} from "@ngx-translate/core";

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  @ViewChild('block') block:ElementRef;
  city : "one";
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public platform:      Platform,
    public translate:     TranslateService,
    public event :        Events,
    public menu: MenuController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
  }

  addComponent() {
    this.block.nativeElement.insertAdjacentHTML('beforeend', '<input type="number" required value="0000000000">');
  }

  openPopup(){
    const modal = this.modalCtrl.create("ChangePasswordPage");
    modal.present();
  }

  onChange(newValue) {
    this.city = newValue;
    if(newValue == "one"){
      console.log(newValue);
      this.platform.setDir('rtl',true);
      localStorage.setItem('storzat_lang',newValue)
      this.translate.setDefaultLang(newValue);
      this.menu.enable(true, 'right');
      this.menu.enable(false, 'left');
      this.translate.use(newValue);
      this.event.publish('change_lang');
      this.translate.setDefaultLang("ar");
      this.translate.use("ar");
    }else{
      this.platform.setDir('ltr',true);
      localStorage.setItem('storzat_lang',newValue);
      this.menu.enable(false, 'right');
      this.menu.enable(true, 'left');
      this.translate.setDefaultLang(newValue);
      this.translate.use(newValue);
      this.event.publish('change_lang');
      this.translate.setDefaultLang("en");
      this.translate.use("en"); 
    }
    // ... do other stuff here ...
  }


}
