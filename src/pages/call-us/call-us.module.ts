import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CallUsPage } from './call-us';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CallUsPage,
  ],
  imports: [
    IonicPageModule.forChild(CallUsPage),
    TranslateModule,
  ],
})
export class CallUsPageModule {}
