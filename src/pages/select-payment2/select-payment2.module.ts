import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectPayment2Page } from './select-payment2';

@NgModule({
  declarations: [
    SelectPayment2Page,
  ],
  imports: [
    IonicPageModule.forChild(SelectPayment2Page),
  ],
})
export class SelectPayment2PageModule {}
