import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ModalController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  pet: string = "plus1";
  iconRight : any = 'right';
  ionViewWillEnter(){
    if(this.platform.isRTL){
       this.iconRight = 'right';
    } else{
     this.iconRight = 'left';
    } 
   
 }
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private platform : Platform,
    public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  openPopup(){
    const modal = this.modalCtrl.create("SelectPaymentPage");
    modal.present();
  }

}
